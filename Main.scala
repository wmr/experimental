//
// unar.scala // Recursive UNARchiver tool
//
// build with:
// scalac main/scala/com/wmrware/unar/Main.scala -cp ../target/dependency/commons-cli-1.2.jar
//
// run with:
// scala -cp commons-cli-1.2.jar:scala-library-2.7.2-rc2.jar:. com.wmrware.unar.Main -path <path> 
//
// +lazy
// Copyright (c) wmr <wmr101@gmail.com>. All Rights Reserved.
//


package com.wmrware.unar

import java.io.File

import org.apache.commons.cli.Options 
import org.apache.commons.cli.Option 
import org.apache.commons.cli.BasicParser 

import scala.language.postfixOps
import scala.util.matching.Regex
import scala.sys.process._
// Try to uncomment these one by one and... surprise!
//import scala.collection.mutable._
//import scala.collection.immutable._

object Main {
    // Don't ask why...
    def getUNZIPCmd(archive: String, target:String): Seq[String] = {
        return Seq("unzip", "-n", archive, "-d", target)
    }

    def getUNRARCmd(archive:String, target:String): Seq[String] = {
        return Seq("unrar", "x", "-o-", archive, target)
    }
    
    // Entry point
    def main(args: Array[String]) {
        
        val RE_ZIP_AR = """.*\.zip$""".r
        val RE_RAR_AR = """.*\.rar$""".r

        // parse command line arguments
        val opts = new Options()
        opts.addOption("path", true, "target path")
        val parser = new BasicParser()
        val cmdLine = parser parse(opts, args, false)

        var path = ""
        for (o <- cmdLine getOptions) {
           if ((o getOpt) == "path") {
             println(o getValue)
             path = o getValue
           }
        }

        var prevSet  = Set[java.io.File]()
        var currentSet = Set[java.io.File]()
        var diffSet = Set[java.io.File]()

        var pathFile = new File(path)
        
        do {
            prevSet = currentSet

            // good luck using infix here
            currentSet = pathFile.listFiles().toSet.filter(
                   x => (RE_ZIP_AR.findFirstIn(x.getAbsolutePath()).nonEmpty 
                       || RE_RAR_AR.findFirstIn(x.getAbsolutePath()).nonEmpty)
            )

            println(currentSet.mkString)

            diffSet = currentSet -- prevSet

            diffSet.foreach( x => 
                    if (RE_ZIP_AR.findFirstIn(x.getAbsolutePath()).nonEmpty) {
                        println("zip")
                        getUNZIPCmd(x.getAbsolutePath(), path).!
                    }
                    else if(RE_RAR_AR.findFirstIn(x.getAbsolutePath()).nonEmpty) {
                        println("rar")
                        getUNRARCmd(x.getAbsolutePath(), path).!
                    }
            )
        } 
        while (diffSet.size > 0)
    }
}

