#!/usr/bin/env python2.7

#
# unar // recursive UNARchival tool
# +pep8
# (c) Copyright 2013 wmr. All Rights Reserved.
#


def parse_args():
    """Parse command line arguments."""
    import argparse
    parser = argparse.ArgumentParser(description='Recursive UNARchival tool')
    parser.add_argument('torrentDir')

    return parser.parse_args()

args = parse_args()


def pathDiffGenerator(path):
    import sets
    prev_file_names = set([])
    set_of_archives = lambda path: set(
        filter(lambda x: (RE_ZIP_AR.match(x)
                          or RE_RAR_AR.match(x)),
               os.listdir(path)))

    new_file_names = file_names = set_of_archives(path) 
    while len(new_file_names) > 0:

        yield new_file_names

        prev_file_names = file_names
        file_names = set_of_archives(path)
        new_file_names = file_names - prev_file_names


def substituteElements(srcList, subsMap):
    return [elem % subsMap for elem in srcList]

import re

RE_RAR_AR = re.compile('.*\.rar$')
RE_ZIP_AR = re.compile('.*\.zip$')

TOOL_UNRAR = ['unrar', 'x', '-o-', '%(archive_path)s', '%(target_path)s']
TOOL_UNZIP = ['unzip', '-n', '%(archive_path)s', '-d', '%(target_path)s']

import os
import subprocess

for file_names in pathDiffGenerator(args.torrentDir):
    print file_names
    for filename in file_names:
        subsMap = {'archive_path': os.path.join(args.torrentDir, filename),
                   'target_path': args.torrentDir}

        if RE_ZIP_AR.match(filename):
            unzip_cmd = substituteElements(TOOL_UNZIP, subsMap)
            subprocess.call(unzip_cmd)

        if RE_RAR_AR.match(filename):
            unrar_cmd = substituteElements(TOOL_UNRAR, subsMap)
            subprocess.call(unrar_cmd)
