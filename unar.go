//
// unar (in go) // recursive UNARchiver tool
// +lazy
// run with:   go run unar.go -path <path>
// build with: go build unar.go
//
// Copyright (c) wmr <wmr101@gmail.com>. All Rights Reserved.
//

package main

import (
        "fmt"
        "flag"
        "os"
        "path"
        "os/exec"
        "regexp"
       )

// You can't use variable := <value> syntax here since it is not within
// a function body. Makes sense, right? (NO!)
var isRARArchive = regexp.MustCompile(`.*\.rar`)
var isZIPArchive = regexp.MustCompile(`.*\.zip`)

var UNRAR_TOOL_ARGS = []string{"unrar", "x", "-o-", "$archive_name", "$target_path"}
const UNRAR_TOOL = "unrar"
var UNZIP_TOOL_ARGS = []string{"unzip", "-n", "$archive_name", "-d", "$target_path"}
const UNZIP_TOOL = "unzip"

// 'sets' are simulated with string->bool maps, 
// this function calculates 'A set' - 'B set' operation
func stringSetMinus(leftSet map[string]bool, rightSet map[string]bool) map[string]bool {
    ret := make(map[string]bool)
    for k, _ := range leftSet {
        if rightSet[k] {
            continue
        } else {
            ret[k] = true
        }
    }

    return ret
}

// Substitutes the parameters marked with $, with the actual values given
func subsArgs(args []string, archive_name string, target_path string) []string {
    ret := make([]string, len(args))
    for index, elem := range args {
       if elem == "$archive_name" {
           ret[index] = archive_name
       } else if elem == "$target_path" {
           ret[index] = target_path
       } else {
           ret[index] = elem
       }

    }
    fmt.Println(ret)
    return ret
}


// Entry point
func main() {

    // parse command line flags
    // this thing sucks compared to argparse, a bit time
   fpath := flag.String("path", "", "target path")
   flag.Parse()

   fmt.Println("path: ", *fpath)

   set := make(map[string]bool)
   prev_set := make(map[string]bool)

   // endless loop, there is no 'while' huhh... just to make things obscure
   for {
       prev_set = set
       set = make(map[string]bool)

       f, err := os.Open(*fpath)
       if err != nil {
           return
       }

       // what the hell is the -1 param??
       list, err := f.Readdir(-1)
       // You need to do this after each and evey function call...
       // and they say this is better then try/catch/finally
       // uh-huh... not so much convinced
       if err != nil {
           return
       }

       err = f.Close()

       if err != nil {
           return
       }

       for _, elem := range list {
           set[elem.Name()] = true
       }
       fmt.Println(set)

       diff_set := stringSetMinus(set, prev_set)
       fmt.Println(diff_set)

       // pull the plug
       if len(diff_set) == 0 {
           return
       }

       for elem, _ := range diff_set {

           if isZIPArchive.Match([]byte(elem)) {
               fmt.Println(elem)

               // setup Cmd object
               cmd := exec.Command(UNZIP_TOOL)
               zip_args := subsArgs(UNZIP_TOOL_ARGS, path.Join(*fpath, elem), *fpath)
               cmd.Args = zip_args

               // run actual command
               output, _ := cmd.CombinedOutput()
               fmt.Println(string(output))
           } else if isRARArchive.Match([]byte(elem)) {
               fmt.Println(elem)

               // setup Cmd object
               cmd := exec.Command(UNRAR_TOOL)
               rar_args := subsArgs(UNRAR_TOOL_ARGS, path.Join(*fpath, elem), *fpath)
               cmd.Args = rar_args

               // run actual command
               output, _ := cmd.CombinedOutput()
               fmt.Println(string(output))
           }

       }
   }
}

