#!/usr/bin/env ruby

# unar.rb // recursive UNARchival tool
# +lazy
# (c) Copyright 2013 wmr. All Rights Reserved. 
#

require 'optparse'
require 'ostruct'
require 'set'

RE_RAR_AR = Regexp.new '.*\.rar$'
RE_ZIP_AR = Regexp.new '.*\.zip$'

TOOL_UNRAR = ['unrar', 'x', '-o-', '%{archive_path}', "%{target_path}"]
TOOL_UNZIP = ['unzip', '-n', "%{archive_path}", '-d', "%{target_path}"]

# Ruby's yield is not a yield. It is something else. X)
# this is more like a language level callback mechanism. 
# yield calls the 'block' parameter given to the function
def path_diff_generator(path)
    previous_dir_stat = Set.new
    set_of_archives = lambda {|fname| (fname =~ RE_RAR_AR or fname =~ RE_ZIP_AR)}
    diff_dir_stat = current_dir_stat = 
                        Set.new(Dir.entries(path).find_all &set_of_archives)
    
    while diff_dir_stat.length > 0
      
      yield (diff_dir_stat)
      
      previous_dir_stat = current_dir_stat
      current_dir_stat = Set.new(Dir.entries(path).find_all &set_of_archives)
      diff_dir_stat = current_dir_stat - previous_dir_stat
    end
end

# Set up command line argument parsing
options = OpenStruct.new

opt_parser = OptionParser.new do |opts|
  opts.banner = "Usage: unar.rb [path]"
  opts.on('-p', '--path PATH', 'target path') do |path|
    options.path = path
  end
end

# Note the exclamation mark. Without that the parsing never fails
# This is some kind of weird method overloading...
opt_parser.parse!

path_diff_generator(options.path) do |diff_dir_stat|
  p diff_dir_stat
  for fname in diff_dir_stat
    puts "\nProcessing: #{fname}"
    subsMap = {:archive_path => File.join(options.path, fname), 
               :target_path => options.path}
             
    if fname =~ RE_ZIP_AR
      system(*(TOOL_UNZIP.map{|x| x % subsMap}))
    end
  
    if fname =~ RE_RAR_AR
      system(*(TOOL_UNRAR.map{|x| x % subsMap}))
    end
  end
end
