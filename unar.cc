// 
// unar // Recursive UNARarchiver
// unar.cc
// +c++11 +lazy
// build with: clang++ -std=c++11 unar.cc -L /usr/local/lib -lboost_regex -lboost_program_options -lboost_filesystem -lboost_system
// (c) Copyright 2013 wmr. All Rights Reserved. 
//

// (std)::
#include <cstdio>

// std::
#include <iostream>
#include <vector>
#include <algorithm>
#include <regex> 

// boost::program_options::
#include <boost/program_options.hpp>
// boost::format::
#include <boost/format.hpp>
// boost::filesystem
#include <boost/filesystem.hpp>

// global constants
static const char*      FLAG_PATH("path");
static const char*      FLAG_HELP("help"); 
static const std::regex RE_RAR_AR(".*\\.rar");
static const std::regex RE_ZIP_AR(".*\\.zip");


static const char* TOOL_UNZIP_FMT("unzip -n \"%s\" -d \"%s\"");
static const char* TOOL_UNRAR_FMT("unrar x -o- \"%s\" \"%s\"");

/////////////////////////////////////////////////
// Executes the given command via popen(3)
void
exec(const std::string& cmd) {
    using std::cout;
    FILE* exec_handle = popen(cmd.c_str(), "r"); // read only mode
    if (!exec_handle) {
        return;
    }
    
    // There must be a way to read this with a stream reader
    char buff[1024]; // read buffer
    while (fgets(buff, sizeof(buff), exec_handle) != NULL) {
        cout << buff;
    }
    cout << '\n';
    pclose(exec_handle);
}


/////////////////////////////////////////////////
// Parses the available command line arguments
boost::program_options::variables_map 
parse_args(int ac, char const* av[]) 
{    
    namespace po = boost::program_options;
    
    using std::cout;
    using boost::format;
    
    // set up available command line flags
    po::options_description desc("Allowed options");
    desc.add_options()
        (FLAG_HELP, "produce help message")
        (FLAG_PATH, po::value<std::string>(), "target path")
    ;
    
    po::variables_map vm;
    po::store(po::parse_command_line(ac, av, desc), vm);
    po::notify(vm);
    
    // print usage
    if (vm.count(FLAG_HELP)) {
        cout << format("%s\n") % desc;
        exit(0);
    }
    
    if (!vm.count(FLAG_PATH)) {
        cout << format("Flag: '%s' is mandatory\n%s\n") % FLAG_PATH % desc;
        exit (-1);
    }
    
    return vm;
}


/////////////////////////////////////////////////
// Entry point
int
main (int argc, char const *argv[]) 
{    
    namespace po = boost::program_options;
    namespace fs = boost::filesystem;

    using std::cout;
    using std::cerr;
    // containers
    using std::set;
    // <algorithm>
    using std::copy;
    using std::transform;
    using std::set_difference;
    using std::back_inserter;
    using std::inserter;
    // <regex>
    using std::regex_match;
    // boost::*
    using boost::format;
    
    try {
        auto vm = parse_args(argc, argv);    
        auto path_str = vm[FLAG_PATH].as<std::string>();
            
        cout << format("Processing: %s\n") % path_str;
        
        fs::path p(path_str);
        if (!fs::exists(p) || !fs::is_directory(p)) {
            cerr << format("The given path is invalid: %s\n") % path_str;
            return -1;
        }
        
        try {
            
            typedef std::string string_t;

            set<string_t> current_set;
            set<string_t> prev_set;
            set<string_t> diff_set;
            
            do {
                typedef fs::path path_t;
        
                // save the current_set to prev_set
                prev_set.clear();
                copy(current_set.begin(), current_set.end(), inserter(prev_set, prev_set.end()));
                
                // produce the current file list in current_set
                current_set.clear();                      
                transform(fs::directory_iterator(p), fs::directory_iterator(), // range 
                          inserter(current_set, current_set.end()),            // inserter
                          [](path_t f) -> string_t { return f.native(); });   // transform
                
                // generate the difference between current_set and diff_set
                diff_set.clear();
                set_difference(current_set.begin(), current_set.end(), // left set range 
                               prev_set.begin(), prev_set.end(),       // right set range
                               inserter(diff_set, diff_set.end()) );   // inserter
            
                // execute the unarchival tasks on specific files
                for (auto &entry : diff_set) {
                    
                    cout << format("%s\n") % entry;
                    
                    if (regex_match(entry, RE_ZIP_AR)) {
                        exec(boost::str(format(TOOL_UNZIP_FMT) % entry % p.native()));
                    }
                    
                    if (regex_match(entry, RE_RAR_AR)) {
                        exec(boost::str(format(TOOL_UNRAR_FMT) % entry % p.native()));
                    }
                }
            
            } while (diff_set.size() > 0);
        }
        catch (const fs::filesystem_error& ex) {
            cerr << format("%s\n") % ex.what();
        }
    
    }
    catch (const po::error& err) {
        cerr << format("%s\n") % err.what();
        exit(-1);
    }
    
    return 0;
}
